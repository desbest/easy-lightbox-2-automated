=== Easy Lightbox 2 Automated ===
Contributors: desbest, Rupert Morris
Donate link: http://paypal.me/tynamiteuk
Requires at least: 3.4.5
Tested up to: 5.7
Requires PHP: 5.3
Stable tag: trunk
License: MIT
Version: 3.1
License URI: https://en.wikipedia.org/wiki/MIT_License
Tags: AJAX, image, images, lightbox, photo, picture

Add Lokesh's Lightbox 2 easily to your website with this plugin.

== Description ==

Used to overlay images on the current page. Lightbox JS v2.2 by [Lokesh Dhakar]( http://www.huddletogether.com/projects/lightbox2/). Features 'auto-lightboxing' of image links, courtesy of [Michael Tyson]( http://atastypixel.com).

This plugin is now maintained by [desbest](http://desbest.com/projects/easy-lightbox-2-automated-automated). Before it was maintained by [Rupert Morris](http://stimul.ca)

== Installation ==

To do a new installation of the plugin, please follow these steps

1. Download the zipped plugin file to your local machine.
2. Unzip the file.
3. Upload the `easy-lightbox-2-automated` folder to the `/wp-content/plugins/` directory.
4. Activate the plugin through the 'Plugins' menu in WordPress.
5. Optionally, go to the Options page and select a new Lightbox colour scheme.

If you have already installed the plugin

1. De-activate the plugin.
2. Download the latest files.
2. Follow the new installation steps.

== Important notice ==
For this plugin to work, you must have the [Cleaner Gallery](https://wordpress.org/plugins/cleaner-gallery/) plugin installed.

== Frequently Asked Questions ==

**Why doesn't it work for me?**

A: Either:

1. For this plugin to work, you must have the [Cleaner Gallery]((https://wordpress.org/plugins/cleaner-gallery/)) plugin installed.
2. You have changed the plugin folder's name to something other than "easy-lightbox-2-automated".
3. The problem is with your Wordpress theme, mangling image display properties. Use another theme, that doesn't interfere with posted images.
4. You have other plugins that conflict with Lightbox 2. Disable your other plugins and see if that helps. If it does, re-enable each plugin, one at a time to see which one is causing the conflict.

**It doesn't work properly in Browser X (Explorer 6, 7, etc)?**

Yes it does. The problem is with your Wordpress theme, mangling image display properties. Use another theme, or hack your theme's Cascading Style Sheets (CSS).

**I made my own Wordpress theme, or heavily hacked another one, and lightbox doesn't work at all, ever.**

You forgot to include wp_header() in your header.php of your Wordpress theme. Be sure to look at the default theme, or other quality themes (eg: standards-compliant XHTML and CSS) to see how they work while hacking your own ;)

If you have read and tried the above, then, and ONLY then, I invite you to post your issues, in detail (include links) to my site.