<?php
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/*
Plugin Name: Easy Lightbox 2 Automated
Plugin URI: http://desbest.com/projects/easy-lightbox-2-automated
Description: Convert all images in your blog posts and pages to work with Lightbox 2. Lightbox 2 is used to overlay a larger size of images on the current page once clicked on, instead of opening them in a new window/popup. Lightbox JS v2.2 by <a href="http://www.huddletogether.com/projects/lightbox2/" title="Lightbox JS v2.2 ">Lokesh Dhakar</a>. This plugin was originally made by Rupert Morris of Stimuli.
Version: 3.1
Author: desbest
Author URI: http://desbest.com
*/

/* Where our theme reside: */
$lightbox_2_theme_path = (dirname(__FILE__)."/Themes");
update_option('lightbox_2_theme_path', $lightbox_2_theme_path);
/* Set the default theme to Black */
add_option('lightbox_2_theme', 'Black');
add_option('lightbox_2_automate', 1);
add_option('lightbox_2_resize_on_demand', 0);

/* use WP_PLUGIN_URL if version of WP >= 2.6.0. If earlier, use wp_url */
if($wp_version >= '2.6.0') {
	$stimuli_lightbox_plugin_prefix = plugins_url( '/', __FILE__ ); /* plugins dir can be anywhere after WP2.6 */
} else {
	$stimuli_lightbox_plugin_prefix = plugins_url( '/', __FILE__ );
}

/* options page (required for saving prefs)*/
$options_page = admin_url().'admin.php?page=easy-lightbox-2-automated/options.php';
/* Adds our admin options under "Options" */
function el2a_lightbox_2_options_page() {
	add_options_page('Lightbox Options', 'Easy Lightbox 2 Automated', 10, 'easy-lightbox-2-automated/options.php');
}

function el2a_lightbox_styles() {
	/* What version of WP is running? */
	global $wp_version;
	global $stimuli_lightbox_plugin_prefix;
    /* The next line figures out where the javascripts and images and CSS are installed,
    relative to your wordpress server's root: */
    $lightbox_2_theme = urldecode(get_option('lightbox_2_theme'));
    $lightbox_style = ($stimuli_lightbox_plugin_prefix."Themes/".$lightbox_2_theme."/lightbox.css");
    // exit($lightbox_style);

    /* Include the lightbox stylesheet */
	wp_enqueue_style("lightbox.css", $lightbox_style);
}

/* Added a code to automatically insert rel="lightbox[nameofpost]" to every image with no manual work. 
If there are already rel="lightbox[something]" attributes, they are not clobbered. 
Michael Tyson, you are a regular expressions god! ;) 
http://atastypixel.com
*/
function el2a_autoexpand_rel_wlightbox ($content) {
	global $post;
	$pattern        = "/(<a(?![^>]*?rel=['\"]lightbox.*)[^>]*?href=['\"][^'\"]+?\.(?:bmp|gif|jpg|jpeg|png)['\"][^\>]*)>/i";
	$replacement    = '$1 rel="lightbox['.$post->ID.']">';
	$content = preg_replace($pattern, $replacement, $content);
	return $content;
}

if (get_option('lightbox_2_automate') == 1){
	add_filter('the_content', 'el2a_autoexpand_rel_wlightbox', 99);
	add_filter('the_excerpt', 'el2a_autoexpand_rel_wlightbox', 99);
}

/* To resize images, or not to resize; that is the question */
$resize_images_or_not = get_option('lightbox_2_resize_on_demand');
if ($resize_images_or_not == 1) {
	$stimuli_lightbox_js = "lightbox-resize.js"; 
} else {
	$stimuli_lightbox_js = "lightbox.js"; 
}

if (!is_admin()) { // if we are *not* viewing an admin page, like writing a post or making a page:
	wp_enqueue_script('lightbox', ($stimuli_lightbox_plugin_prefix.$stimuli_lightbox_js), array('scriptaculous-effects'), '1.8');
}

/* we want to add the above xhtml to the header of our pages: */
add_action('wp_head', 'el2a_lightbox_styles');
add_action('admin_menu', 'el2a_lightbox_2_options_page');
?>
